# Org Mode Presentation

view  [slides](https://reedrichards.gitlab.io/org-mode-presentation/)


Slides for my presentation on [org-mode](https://orgmode.org/) made with org and [org-reveal](https://github.com/yjwen/org-reveal)

source material [here](https://gitlab.com/reedrichards/org-mode-presentation/-/tree/master/src)

